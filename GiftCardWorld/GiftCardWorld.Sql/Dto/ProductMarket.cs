﻿using System;
using GiftCardWorld.Common;

namespace GiftCardWorld.Sql.Dto
{
    public class ProductMarket : IAuditableObject
    {
        public string ProductId { get; set; }
        public string MarketId { get; set; }
        public decimal Price { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
    }
}