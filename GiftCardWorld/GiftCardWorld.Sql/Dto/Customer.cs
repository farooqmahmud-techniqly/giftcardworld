﻿using System;
using GiftCardWorld.Common;

namespace GiftCardWorld.Sql.Dto
{
    public class Customer : IEntity, IAuditableObject
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string Id { get; set; }
    }
}