﻿using System;
using GiftCardWorld.Common;

namespace GiftCardWorld.Sql.Dto
{
    public class PaymentMethod : IEntity, IAuditableObject
    {
        public string CustomerId { get; set; }
        public PaymentType PaymentType { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string Id { get; set; }
    }
}