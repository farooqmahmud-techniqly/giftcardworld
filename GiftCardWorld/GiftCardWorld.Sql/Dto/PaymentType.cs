﻿namespace GiftCardWorld.Sql.Dto
{
    public enum PaymentType
    {
        Paypal,
        VisaCheckout,
        CreditCard
    }
}