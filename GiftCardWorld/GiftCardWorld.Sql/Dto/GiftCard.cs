﻿namespace GiftCardWorld.Sql.Dto
{
    public class GiftCard : Product
    {
        public string ImageUrl { get; set; }
        public string EmailTemplateUrl { get; set; }
        public string MerchantId { get; set; }
    }
}