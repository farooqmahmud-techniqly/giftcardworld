﻿using System;
using GiftCardWorld.Common;

namespace GiftCardWorld.Sql.Dto
{
    public class Merchant : IEntity, IAuditableObject
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string Id { get; set; }
    }
}