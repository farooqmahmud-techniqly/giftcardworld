﻿using System;
using GiftCardWorld.Common;

namespace GiftCardWorld.Sql.Dto
{
    public class Product : IEntity, IAuditableObject
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string Id { get; set; }
    }
}