﻿using System;
using GiftCardWorld.Common;

namespace GiftCardWorld.Sql.Dto
{
    public class Market : IEntity, IAuditableObject
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Currency { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string Id { get; set; }
    }
}