﻿using System;
using System.Collections.Generic;
using GiftCardWorld.Common;

namespace GiftCardWorld.Sql.Dto
{
    public class Purchase : IAuditableObject
    {
        public ICollection<Product> Products { get; set; }
        public string CustomerId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime LastModifiedDate { get; set; }
    }
}