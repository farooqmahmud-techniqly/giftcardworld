﻿namespace GiftCardWorld.Common
{
    public interface IEntity
    {
        string Id { get; set; }
    }
}