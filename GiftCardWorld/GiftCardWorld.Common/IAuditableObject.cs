﻿using System;

namespace GiftCardWorld.Common
{
    public interface IAuditableObject
    {
        DateTime CreatedDate { get; set; }
        DateTime LastModifiedDate { get; set; }
    }
}